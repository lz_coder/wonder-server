package com.gitee.whzzone.admin.system.controller;

import com.gitee.whzzone.admin.system.entity.Role;
import com.gitee.whzzone.admin.system.pojo.dto.RoleDto;
import com.gitee.whzzone.admin.system.pojo.query.RoleQuery;
import com.gitee.whzzone.admin.system.service.RoleService;
import com.gitee.whzzone.web.controller.EntityController;
import com.gitee.whzzone.web.pojo.other.PageData;
import com.gitee.whzzone.web.pojo.other.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author : whz
 * @date : 2022/11/30 10:20
 */
@Api(tags = "角色相关")
@RestController
@RequestMapping("role")
public class RoleController extends EntityController<Role, RoleService, RoleDto, RoleQuery> {

    @Autowired
    private RoleService roleService;

    @ApiOperation("列表")
    @GetMapping("list")
    public Result<List<RoleDto>> list(RoleQuery query){
        return Result.ok("操作成功", roleService.list(query));
    }

    @ApiOperation("分页")
    @GetMapping("page")
    public Result<PageData<RoleDto>> page(RoleQuery query){
        return Result.ok(roleService.page(query));
    }

    @ApiOperation("改变启用状态")
    @PutMapping("/enabledSwitch/{id}")
    public Result enabledSwitch(@PathVariable Integer id) {
        roleService.enabledSwitch(id);
        return Result.ok();
    }

    @ApiOperation("绑定规则")
    @PutMapping("/bindingRule")
    public Result bindingRule(Integer roleId, Integer ruleId) {
        roleService.bindingRule(roleId, ruleId);
        return Result.ok();
    }

    @ApiOperation("取消绑定规则")
    @PutMapping("/unBindingRule")
    public Result unBindingRule(Integer roleId, Integer ruleId) {
        roleService.unBindingRule(roleId, ruleId);
        return Result.ok();
    }
}
