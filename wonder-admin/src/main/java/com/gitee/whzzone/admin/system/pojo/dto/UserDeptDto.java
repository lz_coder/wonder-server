package com.gitee.whzzone.admin.system.pojo.dto;

import com.gitee.whzzone.web.pojo.dto.EntityDto;
import lombok.Data;

/**
 * @author Create by whz at 2023/7/9
 */

@Data
public class UserDeptDto extends EntityDto {

}
